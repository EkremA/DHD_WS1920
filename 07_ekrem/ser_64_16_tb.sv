module ser_64_16_test();

   wire clk;
   reg 	reset_n;
   reg [63:0] data_in;
   wire [15:0] data_out;
   reg 	       valid_in;
   reg 	       stop_in;
   wire        stop_out;
   wire        valid_out;
   

   clock #(.FREQ(500)) CLK1_I (.clk(clk));
   ser_64_16_ver2 SER1_I (.clk(clk), .res_n(reset_n), .valid_in(valid_in), .stop_in(stop_in), .valid_out(valid_out), .stop_out(stop_out), .data_in(data_in), .data_out(data_out));


   initial
     begin
	// invalid inputs
        valid_in <= 1'bx;
        stop_in <= 1'bx;
        data_in <= 64'bx;
        #4
        // pull reset
        res_n <= 1'b0;
        #4
        // valid inputs
        stop_in <= 1'b0;
        data_in <= 64'hC001D00DCAFEFACE;
        # 2
        // start transmitting
        valid_in <= 1'b1;
        #1
        // test stop_in
        stop_in <= 1'b0;
        #2
        stop_in <= 1'b1;
        #8
        // wait until data transfer is completed
        // test another transfer without interruption
        valid_in <= 1'b0;
        #1
        data_in <= 64'hC0CAC01A_ADD511FE;
        #1
        valid_in <= 1'b1;
        #50
        // wait until data transfer is completed
        $finish;
     end
   


   
endmodule // ser_64_16_test
