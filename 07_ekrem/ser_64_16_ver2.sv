module ser_64_16_ver2 (
  input wire clk,
  input wire res_n,
  input wire valid_in,
  input wire stop_in,
  input wire [63:0] data_in,
  output wire stop_out,
  output wire valid_out,
  output reg [15:0] data_out
);

wire mux0;
wire mux1;


// Control path
//internal control vars
wire mux2; //will not be used though, default 0 please
wire g0, g1, g2;

arbiter arbiter_I (.clk(clk), .res_n(res_n),
                    .req0(mux0 & valid_in),
                    .req1(mux1 & valid_in),
                    .req2(mux2 & valid_in),
                    .gnt0(g0),
                    .gnt1(g1),
                    .gnt2(g2));
assign valid_out = g0 | g1 | g2;


// Datapath
logic [15:0] data_out_int;
always_comb begin
  case ({mux1,mux0})
    2'b00: data_out_int = data_in[63:48];
    2'b01: data_out_int = data_in[47:32];
    2'b10: data_out_int = data_in[31:16];
    2'b11: data_out_int = data_in[15:0];
  endcase
end

always @(posedge clk or negedge res_n) begin
  if(~res_n) begin
    data_out <= 16'b0;
  end else begin
    data_out <= data_out_int;
  end
end

endmodule
