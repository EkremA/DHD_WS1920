module arbiter_tb();
	reg gnt0, gnt1, gnt2;
	reg req0, req1, req2;
	reg res_n;
	reg clk;
	
	clkgen #(.FREQ(500)) clk_I (.clk(clk));
	arbiter arbiter_I (.clk(clk), .res_n(res_n), 
						.req0(req0), 
						.req1(req1), 
						.req2(req2), 
						.gnt0(gnt0), 
						.gnt1(gnt1), 
						.gnt2(gnt2));
	

	task updateReqs();
		input [2:0] reqs;
		begin
			req0 = reqs[0];
			req1 = reqs[1];
			req2 = reqs[2];
		end
	endtask


	initial begin
		#100
		$finish;
	end


endmodule