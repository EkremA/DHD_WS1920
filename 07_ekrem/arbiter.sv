module arbiter (
    input wire clk,
    input wire res_n,

    input wire req2,
    input wire req1,
    input wire req0,

    output wire gnt2,
    output wire gnt1,
    output wire gnt0
);

localparam idle = 3'b000;
localparam p0 = 3'b001;
localparam p1 = 3'b010;
localparam p2 = 3'b100;

reg [2:0] state;
reg [2:0] next_state;
assign {gnt2,gnt1,gnt0} = state;

wire [2:0] input_vector;
assign input_vector = {req2,req1,req0};

always @(*) begin
    casex(state)
        idle: begin
            casex (input_vector)
                3'bXX1: next_state = p0;
                3'b000: next_state = idle;
                3'bX10: next_state = p1;
                3'b100: next_state = p2;
            endcase
        end
        p0: begin
            casex (input_vector)
                3'b000: next_state = idle;
                3'b001: next_state = p0;
                3'bX1X: next_state = p1;
                3'b10X: next_state = p2;
            endcase
        end
        p1: begin
            casex (input_vector)
                3'b000: next_state = idle;
                3'b010: next_state = p1;
                3'b0X1: next_state = p0;
                3'b1XX: next_state = p2;
            endcase
        end
        p2: begin
            casex (input_vector)
                3'b000: next_state = idle;
                3'b100: next_state = p2;
                3'bXX1: next_state = p0;
                3'bX10: next_state = p1;
            endcase
        end
        default: next_state = state;
    endcase
end

always @(posedge clk or negedge res_n) begin
    if(!res_n) begin
        state <= idle;
    end else begin
        state <= next_state;
    end
end

`ifdef CAG_ASSERTIONS
idle_Trans_1_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == idle) &&  req0 |=> state == p0);
idle_Trans_16_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == idle) && !req2 && !req1 && !req0 |=> state == idle);
idle_Trans_2_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == idle) &&  req1 && !req0 |=> state == p1);
idle_Trans_3_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == idle) &&  req2 && !req1 && !req0 |=> state == p2);
p0_Trans_10_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p0) && !req2 && !req1 && !req0 |=> state == idle);
p0_Trans_13_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p0) && !req2 && !req1 &&  req0 |=> state == p0);
p0_Trans_4_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p0) &&  req1 |=> state == p1);
p0_Trans_5_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p0) &&  req2 && !req1 |=> state == p2);
p1_Trans_11_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p1) && !req2 && !req1 && !req0 |=> state == idle);
p1_Trans_14_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p1) && !req2 &&  req1 && !req0 |=> state == p1);
p1_Trans_6_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p1) && !req2 &&  req0 |=> state == p0);
p1_Trans_7_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p1) &&  req2 |=> state == p2);
p2_Trans_12_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p2) && !req2 && !req1 && !req0 |=> state == idle);
p2_Trans_15_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p2) &&  req2 && !req1 && !req0 |=> state == p2);
p2_Trans_8_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p2) &&  req0 |=> state == p0);
p2_Trans_9_cond1    : assert property(@(posedge clk) disable iff(!res_n) (state == p2) &&  req1 && !req0 |=> state == p1);

idle_completeness   : assert property(@(posedge clk) disable iff(!res_n) (state == idle) |->
                                       (req0) |
                                       (!req2 & !req1 & !req0) |
                                       (req1 & !req0) |
                                       (req2 & !req1 & !req0)  == 1);
p0_completeness   : assert property(@(posedge clk) disable iff(!res_n) (state == p0) |->
                                       (!req2 & !req1 & !req0) |
                                       (!req2 & !req1 & req0) |
                                       (req1) |
                                       (req2 & !req1)  == 1);
p1_completeness   : assert property(@(posedge clk) disable iff(!res_n) (state == p1) |->
                                       (!req2 & !req1 & !req0) |
                                       (!req2 & req1 & !req0) |
                                       (!req2 & req0) |
                                       (req2)  == 1);
p2_completeness   : assert property(@(posedge clk) disable iff(!res_n) (state == p2) |->
                                       (!req2 & !req1 & !req0) |
                                       (req2 & !req1 & !req0) |
                                       (req0) |
                                       (req1 & !req0)  == 1);
`endif

endmodule

