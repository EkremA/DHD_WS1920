module lzc_lin #(int N_DATA_BITS=16) (input wire [N_DATA_BITS-1:0] bitvector);
	reg count;
	
	always @(*) begin
	count = 0;
        for (ii=N_DATA_BITS-1;ii>=0;ii=ii-1) begin 
            if (bitvector[ii]==1'b0) count = count+1;
        end
        $display("Value of count is %d", count);
    end
endmodule
