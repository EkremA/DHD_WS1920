module clkgen #(parameter FREQ = 500)(output reg clk);
	parameter PERIOD = 1000 / FREQ; //FREQ in MHz, PERIOD in ns
	initial begin
		clk = 0;
		forever begin
			#PERIOD clk = ~clk;
		end
	end
endmodule