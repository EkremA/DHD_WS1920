module ram #(parameter DATAWIDTH = 8, ADDRWIDTH = 8) (
	input wire clk,
	input wire wenable,
	input wire [ADDRWIDTH-1:0] waddress, 
	input wire [DATAWIDTH-1:0] wdata,
	input wire renable,
	input wire [ADDRWIDTH-1:0] raddress,
	output reg [DATAWIDTH-1:0] rdata
	);
	
	parameter numOfAdresses = 2**ADDRWIDTH;	//address space same as available memory addresses of ram
	reg [DATAWIDTH-1:0] mem [numOfAdresses-1:0]; //memory: numOfAdresses x DATAWIDTH Bits

	always @(posedge clk) begin
		if (wenable && !renable) begin //write to an address
				//conversion from binary address to index done implicitly
				mem[waddress] = wdata; 
			end
		else if(!wenable && renable) begin	//read from an address
				rdata = mem[raddress];
			end
		else //undefined, don't do anything
			begin
			end
	end

endmodule