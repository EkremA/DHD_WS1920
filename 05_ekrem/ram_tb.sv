module ram_tb(); 

	//prereqisites
	//============================================
	parameter DATAW = 4;
	parameter ADDRW = 8;

	reg readen;
	reg writeen;
	reg [ADDRW-1:0] waddr;
	reg [ADDRW-1:0] raddr;
	reg [DATAW-1:0] rda;
	reg [DATAW-1:0] wda;
	reg clk;
	clkgen #(.FREQ(500)) clk_I (.clk(clk)); //connect clk var from clkgen to clk in this module
	ram #(.DATAWIDTH(DATAW), .ADDRWIDTH(ADDRW)) ram_I	(.clk(clk), 
														.wenable(writeen),
														.waddress(waddr),
														.wdata(wda),
														.renable(readen),
														.raddress(raddr),
														.rdata(rda));

	//Behaviour and tests
	//============================================
	always @(posedge clk) begin
		readen = 0;
		writeen = 1;
		#4 begin
			waddr = {ADDRW{1'b0}};
			wda = {{ADDRW-4{1'b1}}, {1{4'b1010}}};
		end
		#100
		$finish;
	end

endmodule
