module ser_64_16_tb();
	initial begin 
		clk = 0;
		res_n = 1; //active low
		valid_in = 0;
		stop_in = 0;
		data_in = {64{1'b0}}; //initialise to 64 zeros
		valid_out = 0;
		stop_out = 0;
		data_out = {64{1'b0}}; //same
	end

endmodule