//Serializer 64 to 16 without counter module
module ser_64_16(
	input wire clk,
	input wire res_n,
	input wire valid_in,
	input wire stop_in,
	input wire [63:0] data_in,
	output reg stop_out,
	output reg valid_out,
	output reg [15:0] data_out);	

//initial begin
	//clk = 0;
	//res_n = 1; //active low
	//valid_in = 0;
	//stop_in = 0;
	//data_in = {64{1'b0}}; //initialise to 64 zeros
	//valid_out = 0;
	//stop_out = 0;
	//data_out = {64{1'b0}}; //same
//end

//function for getting block of data_in to be put out 
function data_in_block;
	input block_number; //start counting from zero
	//local variables
	reg pos_bit; //position bit
	begin
		pos_bit = block_number * 16 + (16-1);
		//output
		data_in_block = data_in[pos_bit -: 16]; 
	end
endfunction

always @(*) begin
	//reset behaviour
	if (!res_n) begin
		data_out = {64{1'b0}};
	end
	begin
		data_out = data_in_block(0);
	end
end
endmodule