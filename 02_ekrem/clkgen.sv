module clock_gen #(parameter FREQ=500, parameter INIT=0) (output reg clk);
        initial begin
            clk = INIT;
        end

        while(1)
            #1/FREQ clk = ~clk;
endmodule
