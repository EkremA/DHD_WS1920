module lzc_tb();
    reg [31:0] mybitvector;
    lzc_lin #(32) lzc_lin_I (.bitvector(mybitvector));

    initial begin
        #3
        mybitvector = {17{1'b0}, 2{1'b1}, 13{1'b0}};
        #20        
        $finish;
    end
endmodule
