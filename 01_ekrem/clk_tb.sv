module clk_tb();
    wire clk;
    clkgen #(.FREQ(500)) clk_I (.clk(clk));
    always @*
        #100
        $finish;
endmodule
