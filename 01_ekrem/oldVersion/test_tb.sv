module test();
	wire myclock;
	clkgen #(.PERIOD_LEN(2)) myclock_I (.myclk(myclock));

	initial begin
		#10
		$finish;
	end
endmodule