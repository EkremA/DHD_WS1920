module clkgen #(parameter PERIOD_LEN=2)(output reg myclk);
    always begin
        #PERIOD_LEN myclk = ~myclk;
    end
endmodule